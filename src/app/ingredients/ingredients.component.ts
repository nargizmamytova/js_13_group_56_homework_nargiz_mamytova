import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Ingredient} from "../shared/ingredient";

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent{
  @Output() newConsist = new EventEmitter();
  @Output() deleteIng = new EventEmitter();
  @Input() ingredient: Ingredient[] = [];

  onAddIngredient(i: number) {
   this.ingredient[i].coast += 1;
    this.newConsist.emit(i);


   }
  onDelete(i: number) {
    this.ingredient[i].coast -= 1;

  }
}
