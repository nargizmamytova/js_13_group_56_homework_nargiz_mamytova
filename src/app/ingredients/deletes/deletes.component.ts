import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-deletes',
  templateUrl: './deletes.component.html',
  styleUrls: ['./deletes.component.css']
})
export class DeletesComponent {
  coast = 0;
  imgUrl = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRdn-zbZj5ZQVomCWUod8EhTTYmoUNxxvRkoQ&usqp=CAU';
  @Output() delete = new EventEmitter();

  onDeleteUrl() {
    this.delete.emit();
    this.coast = this.coast - 1;
  }
}
