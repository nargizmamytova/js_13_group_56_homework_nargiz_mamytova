import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent{
  @Output() addIngredient = new EventEmitter();
  @Input() imgUrl = ''
  @Input() name = '';
  color = '';

    onClickIngredient(){
    this.addIngredient.emit();
    this.color = 'color';

  }
}
