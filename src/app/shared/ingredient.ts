export class Ingredient{
  constructor(
    public name: string,
    public imgUrl: string,
    public coast: number,
    public price: number
  ) { }
    getPrice(){
    return this.coast * this.price
    }
}
