import { Component } from '@angular/core';
import { Ingredient } from './shared/ingredient';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 sumPrice = 0;
   allIngredients= [
    new Ingredient( 'Meat',  'https://tekhnolog.com/wp-content/uploads/2017/12/Bez-nazvaniya-2-15.jpg', 0,  50),
     new Ingredient( 'Cheese',  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTv5lK6O6LIXMmDi8JUPfBk194iPvmFZB_Mug&usqp=CAU', 0,  30),
    new Ingredient( 'Salad', 'https://calorizator.ru/sites/default/files/imagecache/product_512/product/salad.jpg',  0,  5),
    new Ingredient( 'Bacon', 'https://xcook.info/wp-content/cache/thumb/e8/fc3f2675ae512e8_320x200.jpg',  0,  30)
  ]
  arrayName: string[]= []
  onNewIngredient(i: number){
     this.arrayName = [];
     this.allIngredients.forEach(ingName => {
       for(let i = 0; i < ingName.coast; i++){
         this.arrayName.push(ingName.name);
       }
     })
    console.log(this.arrayName);
     this.getSumPrice();
  }
  getSumPrice(){
    this.sumPrice = 0;
    this.allIngredients.forEach(forSum => {
      this.sumPrice += forSum.getPrice();
    })

  }
  onDeleteIngredient(i: number){
    this.arrayName = [];
    this.allIngredients.forEach(ingName => {
      for(let i = 0; i < ingName.coast; i--){
        this.arrayName.splice(i);
      }
    })
    console.log(this.arrayName);
    this.getSumPrice();
  }

}
