import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { ImagesComponent } from './ingredients/images/images.component';
import { CoastComponent } from './ingredients/coast/coast.component';
import { DeletesComponent } from './ingredients/deletes/deletes.component';
import { BurgerComponent } from './burger/burger.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    IngredientsComponent,
    ImagesComponent,
    CoastComponent,
    DeletesComponent,
    BurgerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
