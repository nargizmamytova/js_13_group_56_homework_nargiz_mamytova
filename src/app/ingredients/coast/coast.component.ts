import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './coast.component.html',
  styleUrls: ['./coast.component.css']
})
export class CoastComponent {
  @Input() coast: number = 0;

  @Output() changeCoast = new EventEmitter();


}
